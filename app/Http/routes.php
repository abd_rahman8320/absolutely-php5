<?php
// use View;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
date_default_timezone_set('Asia/Jakarta');
Route::get('test', 'LoginController@index');

Route::get('/', 'HomeController@index');

Route::get('/house-reel', 'HomeController@house_reel');
Route::get('/award-kami', 'HomeController@award_kami');

Route::get('login', 'LoginController@login');
Route::post('login', 'LoginController@loginProcess');
Route::get('logout', 'LoginController@logout');

Route::group(['prefix' => 'admin-panel', 'middleware' => 'auth.admin'], function() {
    Route::get('/', function () {
        return redirect('admin-panel/carousel');
    });

    Route::resources([
        'carousel' => 'CarouselController',
        'house-reel' => 'HouseReelController',
        'clients' => 'ClientController',
        'settings' => 'SettingsController',
        'award' => 'AwardController'
    ]);

    Route::get('messages', 'MessageController@index');
    Route::delete('messages/{id}', 'MessageController@destroy');
    
    Route::get('change-password', 'LoginController@viewChangePassword');
    Route::put('change-password', 'LoginController@changePassword');
});

Route::post('messages', 'MessageController@store');