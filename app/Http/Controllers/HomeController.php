<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carousel;
use App\Client;
use App\HouseReel;
use App\HouseReelCategory;
use App\Setting;
use App\Award;
use App\AwardCategory;

class HomeController extends Controller
{
    protected $setting;

    public function __construct()
    {
        $this->setting = Setting::find(1);
    }

    public function index()
    {
        $carousels = Carousel::where('status', 1)->orderBy('order', 'asc')->get();
        $clients = Client::get();

        return view('landing-page.index', [
            'carousels' => $carousels,
            'clients' => $clients,
            'setting' => $this->setting
        ]);
    }

    public function house_reel()
    {
        $house_reel_categories = HouseReelCategory::with(['house_reel' => function($q) {
            $q->where('status', 1)
            ->orderBy('order', 'asc');
        }])
        ->has('house_reel')
        ->orderBy('order', 'asc')
        ->get();

        return view('landing-page.components.house-reel-index', [
            'house_reel_categories' => $house_reel_categories,
            'setting' => $this->setting
        ]);
    }

    public function award_kami()
    {
        $award_categories = AwardCategory::with(['award' => function($q) {
            $q->orderBy('order', 'asc');
        }])
        ->has('award')
        ->get();

        return view('landing-page.components.award-index', [
            'setting' => $this->setting,
            'award_categories' => $award_categories
        ]);
    }
}
