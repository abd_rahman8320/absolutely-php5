<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\GlobalTrait;
use App\Client;
use DB;

class ClientController extends Controller
{
    use GlobalTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Client::get();

        return view('admin.page.clients.main', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.page.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->input('name');
        $image = $request->file('image');

        try {
            DB::beginTransaction();

            $destination = public_path().'/assets/clients/';
            $filename = $this->generateRandomString(32).'.'.$image->getClientOriginalExtension();
            $upload = $image->move($destination, $filename);

            $source = $destination.$filename;

            $client = new Client;
            $client->name = $name;
            $client->image = $filename;
            $client->save();

            DB::commit();

            return redirect('admin-panel/clients');
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Client::find($id);

        return view('admin.page.clients.edit', [
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->input('name');
        $image = $request->file('image');

        try {
            DB::beginTransaction();

            if ($image != null) {
                $destination = public_path().'/assets/clients/';
                $filename = $this->generateRandomString(32).'.'.$image->getClientOriginalExtension();
                $upload = $image->move($destination, $filename);

                $source = $destination.$filename;
            }

            $client = Client::find($id);
            $client->name = $name;
            if (isset($filename)) {
                unlink('assets/clients/'.$client->image);
                $client->image = $filename;
            }
            $client->save();

            DB::commit();

            return redirect('admin-panel/clients');
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);

        unlink('assets/clients/'.$client->image);

        $client->delete();

        return redirect('admin-panel/clients');
    }
}
