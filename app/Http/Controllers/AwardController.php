<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Award;
use App\AwardCategory;
use DB;
use App\Traits\GlobalTrait;

class AwardController extends Controller
{
    use GlobalTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Award::orderBy('order', 'asc')->get();

        return view('admin.page.award.main', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = AwardCategory::get();
        $order = Award::max('order') + 1;

        return view('admin.page.award.create', [
            'categories' => $categories,
            'order' => $order
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = $request->input('category');
        $category_name = $request->input('category_name');
        $name = $request->input('name');
        $image = $request->file('image');
        $award = $request->input('award');
        $title = $request->input('title');
        $brand_client = $request->input('brand_client');
        $agency = $request->input('agency');
        $director = $request->input('director');
        $category_award = $request->input('category_award');
        $order = $request->input('order');

        try {
            DB::beginTransaction();

            $destination = public_path().'/assets/award/';
            $filename = $this->generateRandomString(32).'.'.$image->getClientOriginalExtension();
            $upload = $image->move($destination, $filename);

            $source = $destination.$filename;

            if ($category_name != '') {
                $cat = new AwardCategory;
                $cat->name = $category_name;
                $cat->order = AwardCategory::max('order') + 1;
                $cat->save();
                $category = $cat->id;
            }else {
                $check_category = AwardCategory::find($category);
                if ($check_category == null) {
                    return 'Kategori Tidak Ditemukan. Masukan Kategori Terlebih Dahulu';
                }
            }

            $check_order = Award::where('order', $order)->first();
            if ($check_order != null) {
                $reorder_awards = Award::where('order', '>=', $order)->orderBy('order', 'asc')->get();
                $order_before = $order;
                foreach ($reorder_awards as $reorder) {
                    $reorder->order = $order_before + 1;
                    $reorder->save();

                    $order_before++;
                }
            }

            $award_input = new Award;
            $award_input->award_category_id = $category;
            $award_input->name = $name;
            $award_input->image = $filename;
            $award_input->award = $award;
            $award_input->title = $title;
            $award_input->brand_client = $brand_client;
            $award_input->agency = $agency;
            $award_input->director = $director;
            $award_input->category_award = $category_award;
            $award_input->order = ($order != null && $order != 0) ? $order : Award::max('order') + 1;
            $award_input->save();

            DB::commit();

            return redirect('admin-panel/award');
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Award::find($id);
        $categories = AwardCategory::get();

        return view('admin.page.award.edit', [
            'data' => $data,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = $request->input('category');
        $category_name = $request->input('category_name');
        $name = $request->input('name');
        $image = $request->file('image');
        $award = $request->input('award');
        $title = $request->input('title');
        $brand_client = $request->input('brand_client');
        $agency = $request->input('agency');
        $director = $request->input('director');
        $category_award = $request->input('category_award');
        $order = $request->input('order');

        try {
            DB::beginTransaction();
            
            if ($image != null) {
                $destination = public_path().'/assets/award/';
                $filename = $this->generateRandomString(32).'.'.$image->getClientOriginalExtension();
                $upload = $image->move($destination, $filename);
                $source = $destination.$filename;
            }


            if ($category_name != '') {
                $cat = new AwardCategory;
                $cat->name = $category_name;
                $cat->order = AwardCategory::max('order') + 1;
                $cat->save();
                $category = $cat->id;
            }else {
                $check_category = AwardCategory::find($category);
                if ($check_category == null) {
                    return 'Kategori Tidak Ditemukan. Masukan Kategori Terlebih Dahulu';
                }
            }

            $award_input = Award::find($id);

            $check_order = Award::where('order', $order)->first();
            if ($check_order != null && $id != $check_order->id) {
                if ($award_input->order > $order) {
                    $reorder_awards = Award::where('order', '>=', $order)->where('id', '!=', $id)->orderBy('order', 'asc')->get();
                    $order_before = $order;
                    foreach ($reorder_awards as $reorder) {
                        $reorder->order = $order_before + 1;
                        $reorder->save();
    
                        $order_before++;
                    }
                }else {
                    $check_order->order = $award_input->order;
                    $check_order->save();
                }
            }

            $award_input->award_category_id = $category;
            $award_input->name = $name;

            if (isset($filename)) {
                unlink('assets/award/'.$award_input->image);
                $award_input->image = $filename;
            }

            $award_input->award = $award;
            $award_input->title = $title;
            $award_input->brand_client = $brand_client;
            $award_input->agency = $agency;
            $award_input->director = $director;
            $award_input->category_award = $category_award;
            $award_input->order = ($order != null && $order != 0) ? $order : $award_input->order;
            $award_input->save();

            DB::commit();

            return redirect('admin-panel/award');
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $award = Award::find($id);
            $award->delete();

            return redirect('admin-panel/award');
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
