<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\GlobalTrait;
use App\Carousel;
use DB;

class CarouselController extends Controller
{
    use GlobalTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Carousel::orderBy('order', 'asc')->get();

        return view('admin.page.carousel.main', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $order = Carousel::max('order') + 1;
        return view('admin.page.carousel.create', [
            'order' => $order
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('image');
        $order = $request->input('order');
        $status = $request->input('status');

        try {
            DB::beginTransaction();

            $destination = public_path().'/assets/carousel/';
            $filename = $this->generateRandomString(32).'.'.$image->getClientOriginalExtension();
            $upload = $image->move($destination, $filename);

            $source = $destination.$filename;
            // $this->compress($source, $source, 50);

            $check_order = Carousel::where('order', $order)->first();
            if ($check_order != null) {
                $check_order->order = Carousel::max('order') + 1;
                $check_order->save();
            }

            $carousel = new Carousel;
            $carousel->image = $filename;
            $carousel->order = ($order != null && $order != 0) ? $order : Carousel::max('order') + 1;
            $carousel->status = ($status != null) ? $status : 1;
            $carousel->save();

            DB::commit();

            return redirect('admin-panel/carousel');
        } catch (\Exception $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Carousel::find($id);

        return view('admin.page.carousel.edit', [
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = $request->file('image');
        $order = $request->input('order');
        $status = $request->input('status');

        try {
            DB::beginTransaction();

            if ($image != null) {
                $destination = public_path().'/assets/carousel/';
                $filename = $this->generateRandomString(32).'.'.$image->getClientOriginalExtension();
                $upload = $image->move($destination, $filename);

                $source = $destination.$filename;
                // $this->compress($source, $source, 50);
            }

            $carousel = Carousel::find($id);

            $check_order = Carousel::where('order', $order)->first();
            if ($check_order != null) {
                $check_order->order = $carousel->order;
                $check_order->save();
            }

            if (isset($filename)) {
                unlink(public_path().'/assets/carousel/'.$carousel->image);
                $carousel->image = $filename;
            }

            $carousel->order = ($order != null && $order != 0) ? $order : $carousel->order;
            $carousel->status = ($status != null) ? $status : $carousel->status;
            $carousel->save();

            DB::commit();

            return redirect('admin-panel/carousel');
        } catch (\Exception $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carousel = Carousel::find($id);

        unlink('assets/carousel/'.$carousel->image);

        $carousel->delete();

        return redirect('admin-panel/carousel');
    }
}
