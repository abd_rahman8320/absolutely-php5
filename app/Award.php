<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Award extends Model
{
    public function category()
    {
        return $this->belongsTo('App\AwardCategory', 'award_category_id');
    }
}
