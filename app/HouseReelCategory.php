<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HouseReelCategory extends Model
{
    public function house_reel()
    {
        return $this->hasMany('App\HouseReel', 'category_id');
    }
}
