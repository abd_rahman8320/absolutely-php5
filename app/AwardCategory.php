<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AwardCategory extends Model
{
    public function award()
    {
        return $this->hasMany('App\Award', 'award_category_id');
    }
}
