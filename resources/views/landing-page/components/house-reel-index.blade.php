@include('landing-page.components.head')

<body>
    <div class="parallax-content banner-top">
        <div class="position-relative">
            <div class="logo-absolute">
                <a href="/">
                    <img src="{{url('vanilla/img/images/logo.png')}}" alt="logo-absolute">
                </a>
            </div>
        </div>

        <div class="col-md-12" style="padding-top: 44vh;">
            <div class="text-house-reel">
                <h4 class="font-pragmatica font-title">Reels Kami</h4>
                <div class="line-seperate"></div>
                <p class="font-pragmatica font-desc">
                    No project is too big or too small, every project deserves our ABSOLUTE attention
                </p>
            </div>
        </div>
    </div>
    <div class="house-reel-section text-center">
        @foreach($house_reel_categories as $category)
            <h3 class="display-3 text-center">{{$category->name}}</h3>
            <div class="line-seperate diff"></div>
            @php
                $i = 0;
            @endphp
            @foreach($category->house_reel as $house_reel)
                @if($i == 3)
                    @php
                        $i = 0;
                    @endphp
                @endif

                @if($i == 0)
                <div class="card-group px-3" data-aos="fade-up" data-aos-easing="ease-in-sine">
                @endif
                    <div class="card cursor-pointer">
                        <img class="img-thumbnail-reel" src="{{$house_reel->thumbnail}}" alt="logo" onclick="showModal({{$house_reel->id}})">
                    </div>
                @if($i == 2 )
                </div>
                @endif
                @php 
                    $i++;
                @endphp
            @endforeach
            <br>
        @endforeach
    </div>

    @foreach($house_reel_categories as $category)
        @foreach($category->house_reel as $house_reel)
            @include('landing-page.components.modal', ['url' => $house_reel->url, 'id' => $house_reel->id])
        @endforeach
    @endforeach

    <div class="d-flex justify-content-center mb-5">
        <div class="primary-button">
            <a class="font-pragmatica font-desc reel-button" href="{{$setting->youtube}}" target="_blank">Show All</a>
        </div>
    </div>
    


    @include('landing-page.components.footer', ['status' => 'house-reel'])

    @include('landing-page.components.script')
</body>
</html>