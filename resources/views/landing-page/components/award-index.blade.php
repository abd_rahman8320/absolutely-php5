@include('landing-page.components.head')
<body>
    <div class="parallax-content banner-top-2">
        <div class="position-relative">
            <div class="logo-absolute">
                <a href="/">
                    <img src="{{url('vanilla/img/images/logo.png')}}" alt="logo-absolute">
                </a>
            </div>
        </div>

        <div class="col-md-12" style="padding-top: 44vh;">
            <div class="text-house-reel">
                <h4 class="font-pragmatica font-title">Penghargaan Kami</h4>
                <div class="line-seperate"></div>
                <p class="font-pragmatica font-desc">
                    No project is too big or too small, every project deserves our ABSOLUTE attention
                </p>
            </div>
        </div>
    </div>
    <div class="p-5">
        @foreach($award_categories as $category)
            <h3 class="display-3 text-center">{{$category->name}}</h3>
            <div class="line-seperate diff"></div>
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Award Name</th>
                        <th scope="col">Award</th>
                        <th scope="col">Title</th>
                        <th scope="col">Brand / Client</th>
                        <th scope="col">Agency</th>
                        <th scope="col">Director</th>
                        <th scope="col">Category</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($category->award as $award)
                    <tr>
                        <td scope="row">
                            <img src="/assets/award/{{$award->image}}" alt="{{$award->name}}" style="max-height:100px">
                        </td>
                        <td class="align-middle text-left">{{$award->award}}</td>
                        <td class="align-middle text-left">{{$award->title}}</td>
                        <td class="align-middle text-left">{{$award->brand_client}}</td>
                        <td class="align-middle text-left">{{$award->agency}}</td>
                        <td class="align-middle text-left">{{$award->director}}</td>
                        <td class="align-middle text-left">{{$award->category_award}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @endforeach
    </div>

    @include('landing-page.components.footer', ['status' => 'house-reel'])

    @include('landing-page.components.script')
</body>
</html>