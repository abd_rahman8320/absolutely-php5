@include('landing-page.components.head')

<body>
    @include('landing-page.components.sidebar')

    <div class="first-section position-relative" id="home">
        <div class="logo-absolute">
            <a href="/">
                <img src="{{url('vanilla/img/images/logo.png')}}" alt="logo-absolute">
            </a>
        </div>
        <div class="slider-image">
            @foreach($carousels as $carousel)
            <div class="content-slider">
                <img class="w-100" src="{{url('assets/carousel/'.$carousel->image)}}" height="100%" alt="logo">
            </div>
            @endforeach

        </div>
    </div>


    <div class="service-content" id="services">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="service-item">
                        <h4 class="font-pragmatica font-title">Tentang Kami</h4>
                        <div class="line-dec"></div>
                        <p class="font-pragmatica font-desc">
                            Absolute Pictures didirikan pada tahun 2005 oleh Rosman Mohamed. Sejak itu, kami telah membantu
                            banyak klien menyelesaikan pekerjaannya bersama Absolute Pictures.
                            Misi kami adalah membantu semua brand dari seluruh dunia untuk berproduksi bersama Absolute
                            Pictures. Kami membantu brand untuk mendapatkan kualitas internasional dan efisiensi
                            kreatif yang tinggi.
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>


   <div class="parallax-content house-reel" id="house-reel">
       <div class="container">
           <div class="row">
               <div class="col-md-12">
                <div class="text-house-reel">
                    <h4 class="text-center font-pragmatica font-title">Reels Kami</h4>
                    <div class="line-dec"></div>
                    <p class="font-pragmatica font-desc">
                        Kami sangat senang untuk dapat bergabung dalam
                        project dari awal hingga akhir, kami akan
                        membantu mulai pre-production, production and
                        post-production memastikan seluruh project akan
                        selesai tepat pada waktunya
                    </p>
                    <div class="mt-3 mt-2">
                        <div class="primary-button">
                            <a class="font-pragmatica font-desc reel-button" href="/house-reel">Lihat Reels</a>
                        </div>
                    </div>
                </div>
               </div>
           </div>
       </div>
   </div>

    <div class="wrapper-award award" id="award">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-house-reel">
                        <h4 class="text-center font-pragmatica font-title">Penghargaan Kami</h4>
                        <div class="line-dec"></div>
                        <p class="font-pragmatica font-desc">
                            Kami sangat senang untuk dapat bergabung dalam
                            project dari awal hingga akhir, kami akan
                            membantu mulai pre-production, production and
                            post-production memastikan seluruh project akan
                            selesai tepat pada waktunya
                        </p>
                        <div class="mt-3 mt-2">
                            <div class="primary-button">
                                <a class="font-pragmatica font-desc reel-button" href="/award-kami">Lihat Award</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="our-clients-section" id="our-clients">
        <h1 class="display-3 text-center mb-5 font-pragmatica">Client Kami</h1>
        <div class="row">
            <div class="col-12">
                @foreach($clients as $client)
                <img src="{{url('assets/clients/'.$client->image)}}" alt="{{$client->name}}" class="img-clients">
                @endforeach
            </div>
        </div>
    </div>


    <div class="parallax-content contact-content" id="contact-us">
        <div class="container">
            <div class="row">
            <div class="col-md-6">
                <div class="contact-form">
                    <div class="row">
                        <form id="contact" action="" method="post">
                            <div class="row">
                                <input type="hidden" type="text" name="_token" value="{{csrf_token()}}" id="token">
                                <div class="col-md-12">
                                    <fieldset>
                                    <input name="name" type="text" class="form-control" id="name" placeholder="Your name...">
                                    </fieldset>
                                </div>
                                <div class="col-md-12">
                                    <fieldset>
                                    <input name="email" type="email" class="form-control" id="email" placeholder="Your email..." required="">
                                    </fieldset>
                                </div>
                                <div class="col-md-12">
                                    <fieldset>
                                    <textarea name="message" rows="6" class="form-control" id="message" placeholder="Your message..." required=""></textarea>
                                    </fieldset>
                                </div>
                                <div class="col-md-12">
                                    <fieldset>
                                    <button type="button" id="form-submit" class="btn" onclick="sendMessage()">Send Message</button>
                                    <button type="submit" style="display:none" id="btn-submit"></button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="map">
                <!-- How to change your own map point
                        1. Go to Google Maps
                        2. Click on your location point
                        3. Click "Share" and choose "Embed map" tab
                        4. Copy only URL and paste it within the src="" field below
                -->

                    <iframe src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=Jalak%20X,%20Pondok%20Ranji+(My%20Business%20Name)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed" width="100%" height="390" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-12">
                <div class="p-3 mb-5" style="background-color: rgba(0, 0, 0, 0.5);">
                    <h3 class="p-3" style="color: #fff">
                    <b> Office </b> <br>
                    Email : absolutepictures@gmail.com <br>
                    Phone : +62 21 7366997 <br>
                    Address : Komplek DPR, Jl Jalak 10 Kav 10, Pondok Ranji, Tangerang Selatan, Banten. <br> <br>
                    
                    <b>Executive Producer : </b> <br>
                    Rosman Mohammed <br>
                    Email : absolute_rosman@yahoo.com
                    </h3>
                </div>
            </div>
            </div>
        </div>
    </div>

    @include('landing-page.components.footer', ['status' => 'landing'])

    @include('landing-page.components.script')

</body>
</html>
