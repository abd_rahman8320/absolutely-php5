<!-- Bootstrap core JavaScript-->
<script src="{{url('sbadmin/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{url('sbadmin/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{url('sbadmin/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{url('sbadmin/js/sb-admin-2.min.js')}}"></script>