<!DOCTYPE html>
<html lang="en">

<head>
@include('admin.component.head', ['title' => $title])

@yield('content_head')
</head>


<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        @include('admin.component.sidebar')

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                @include('admin.component.topbar')

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    @include('admin.component.page-heading', ['title' => $title])

                    @yield('content')

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            @include('admin.component.footer')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    @include('admin.component.scroll-top-button')

    <!-- Logout Modal-->
    @include('admin.component.modal-logout')

    <!-- Script -->
    @include('admin.component.script')

    @yield('content_script')

</body>

</html>