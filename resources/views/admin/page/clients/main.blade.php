@extends('admin.index', ['title' => 'Admin | Clients'])

@section('content_head')

<!-- Custom styles for this page -->
<link href="{{url('sbadmin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">

@stop

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col-6">
                <h6 class="m-0 font-weight-bold text-primary">Data Clients</h6>
            </div>
            <div class="col-6">
                <a href="{{url('admin-panel/clients/create')}}">
                    <button class="float-right btn btn-primary" title="Add Client">
                        <i class="fas fa-plus"></i>
                    </button>
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @for($i=0;$i<$data->count();$i++)
                        <tr>
                            <td>{{$i+1}}</td>
                            <td>{{$data[$i]->name}}</td>
                            <td>
                                <img src="/assets/clients/{{$data[$i]->image}}" alt="{{$data[$i]->image}}" width="100px" height="100px">
                            </td>
                            <td>{{$data[$i]->created_at}}</td>
                            <td>{{$data[$i]->updated_at}}</td>
                            <td>
                                <div class="row">
                                    <div class="col-3">
                                        <a href="{{url('admin-panel/clients/'.$data[$i]->id.'/edit')}}">
                                            <button class="btn btn-primary" title="Edit">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        </a>
                                    </div>
                                    <div class="col-3">
                                        <form action="{{url('admin-panel/clients/'.$data[$i]->id)}}" method="POST">
                                            {{csrf_field()}}
                                            {{method_field('DELETE')}}

                                            <button type="submit" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></button>
                                        </form>
                                    </div>
                                </div>
                                
                            </td>
                        </tr>
                        @endfor
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('content_script')

<!-- Page level plugins -->
<script src="{{url('sbadmin/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('sbadmin/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{url('sbadmin/js/demo/datatables-demo.js')}}"></script>
@stop