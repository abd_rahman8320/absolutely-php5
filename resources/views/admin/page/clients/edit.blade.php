@extends('admin.index', ['title' => 'Admin | Client'])

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col-6">
                <h6 class="m-0 font-weight-bold text-primary">Edit Client</h6>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form action="{{url('admin-panel/clients/'.$data->id)}}" method="POST" enctype="multipart/form-data" class="col-3">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" name="name" value="{{$data->name}}" class="form-control">
            </div>

            <div class="form-group">
                <label for="image">Image:</label>
                <input type="file" name="image" value="Upload File" class="form-control" accept="image/*">
                <img src="/assets/clients/{{$data->image}}" alt="{{$data->image}}" width="100px" height="100px">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@stop