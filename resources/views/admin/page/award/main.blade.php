@extends('admin.index', ['title' => 'Admin | Award'])

@section('content_head')

<!-- Custom styles for this page -->
<link href="{{url('sbadmin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">

@stop

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col-6">
                <h6 class="m-0 font-weight-bold text-primary">Data Award</h6>
            </div>
            <div class="col-6">
                <a href="{{url('admin-panel/award/create')}}">
                    <button class="float-right btn btn-primary" title="Add Carousel">
                        <i class="fas fa-plus"></i>
                    </button>
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Category</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Award</th>
                        <th>Title</th>
                        <th>Brand / Client</th>
                        <th>Agency</th>
                        <th>Director</th>
                        <th>Category Award</th>
                        <th>Order</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No.</th>
                        <th>Category</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Award</th>
                        <th>Title</th>
                        <th>Brand / Client</th>
                        <th>Agency</th>
                        <th>Director</th>
                        <th>Category Award</th>
                        <th>Order</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @for($i=0;$i<$data->count();$i++)
                        <tr>
                            <td>{{$i+1}}</td>
                            <td>{{$data[$i]->category->name}}</td>
                            <td>{{$data[$i]->name}}</td>
                            <td>
                                <img src="/assets/award/{{$data[$i]->image}}" alt="{{$data[$i]->image}}" width="100px" height="100px">
                            </td>
                            <td>{{$data[$i]->award}}</td>
                            <td>{{$data[$i]->title}}</td>
                            <td>{{$data[$i]->brand_client}}</td>
                            <td>{{$data[$i]->agency}}</td>
                            <td>{{$data[$i]->director}}</td>
                            <td>{{$data[$i]->category_award}}</td>
                            <td>{{$data[$i]->order}}</td>
                            <td>{{$data[$i]->created_at}}</td>
                            <td>{{$data[$i]->updated_at}}</td>
                            <td>
                                <div class="row">
                                    <div class="col-3">
                                        <a href="{{url('admin-panel/award/'.$data[$i]->id.'/edit')}}">
                                            <button class="btn btn-primary" title="Edit">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        </a>
                                    </div>
                                    <div class="col-3">
                                        <form action="{{url('admin-panel/award/'.$data[$i]->id)}}" method="POST">
                                            {{csrf_field()}}
                                            {{method_field('DELETE')}}

                                            <button type="submit" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></button>
                                        </form>
                                    </div>
                                </div>
                                
                            </td>
                        </tr>
                        @endfor
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('content_script')

<!-- Page level plugins -->
<script src="{{url('sbadmin/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('sbadmin/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{url('sbadmin/js/demo/datatables-demo.js')}}"></script>
@stop