@extends('admin.index', ['title' => 'Admin | Carousel'])

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <div class="row">
            <div class="col-6">
                <h6 class="m-0 font-weight-bold text-primary">Edit Carousel</h6>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form action="{{url('admin-panel/carousel/'.$data->id)}}" method="POST" enctype="multipart/form-data" class="col-3">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group">
                <label for="image">Image:</label>
                <input type="file" name="image" value="Upload File" class="form-control" accept="image/*">
                <img src="/assets/carousel/{{$data->image}}" alt="{{$data->image}}" width="100px" height="100px">
            </div>

            <div class="form-group">
                <label for="order">Order</label>
                <input type="number" name="order" value="{{$data->order}}" class="form-control">
            </div>

            <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control" name="status" id="status">
                    <option value="1" @if($data->status == 1) selected @endif>Active</option>
                    <option value="0" @if($data->status == 0) selected @endif >Non-Active</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@stop